import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuariosModule } from './usuarios/usuarios.module';
import { SillasModule } from './sillas/sillas.module';
import { EventosModule } from './eventos/eventos.module';
require('dotenv').config()

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DATABASE_HOST,
      port: 5432,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      extra: {
        ssl: {
          rejectUnauthorized: false,
        },
      },
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: false,
      retryDelay: 5432,
      retryAttempts: 10
    }),
    SillasModule,
    UsuariosModule,
    EventosModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
